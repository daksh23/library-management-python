import os
import time

class Library:

    def __init__(self,name,books):
        self.name = name
        self.books = books

    def displayChoice(self):
        print("=====MY LIBRARY=====");
        print("-> Press 1 for Display Books")
        print("-> Press 2 for Add Book")
        print("-> Press 3 for Lend Book")
        print("-> Press 4 for Return Book")
        return input("Enter Your Choice: ")

    def clearDisplay(self):
        time.sleep(5)
        os.system('cls' if os.name == 'nt' else 'clear')

    def displayBooks(self):
        i = 1
        print("=====BOOK LIST=====");
        for key,item in self.books.items():
            print(i,key)
            i += 1
        print("\n")

    def addBook(self):
        book = input("Enter Book Name: \t").upper()
        self.books.update({book : ""})
        print("Book saved successfully.")
        self.clearDisplay()

    def lendBook(self):
        name = input("Which book you want: ").upper()
        book = self.books.get(name.upper(),"null")
        if(book == "null"):
            print("This book is not available.")
        elif(book != ""):
            print("This book is already occupied.")
        else:
            c_name = input("Please enter your name: ")
            self.books.update({name: c_name})
            print("Book issued successfully.")
        self.clearDisplay()

    def returnBook(self):
        name = input("Book name: ")
        book = self.books.get(name.upper(), "null")
        if (book == "null"):
            print("This book is not available.")
        elif (book == ""):
            print("This book is not issued.")
        else:
            self.books.update({book: ''})
            print("Book returned successfully.")
            self.clearDisplay()

book_dir = {
    "THE LAST HOPE": "",
    "LIFE OF PIE": "",
}

library = Library("MyLibrary",book_dir)
while True:
    choice = library.displayChoice()
    if choice == "1":
        library.displayBooks()
    elif choice == "2":
        library.addBook()
    elif choice == "3":
        library.lendBook()
    elif choice == "4":
        library.returnBook()
    else:
        print("Wrong Choice !")